$(document).ready(function(){

  $(document).on('mousedown', '.module', function(e){
      $('iframe').css({'pointer-events':'none'});
  });

  $(document).on('mouseup', '.module', function(e){
      $('iframe').css({'pointer-events':'all'});
  });
});

$(document).on('dblclick', '.draggable', function(event){
  var curr_module = $(event.currentTarget);

  if(curr_module.parent().prop("id") == "overlay"){
    var e = jQuery.Event( "mousedown", {
    which: 1,
      pageY: curr_module.offset().top,
      pageX: curr_module.offset().left
    });
    //click_cursor(e);
    find_cursor_loc(e);
    var curr_module = curr_module.detach();
    curr_module.css({"transform":"translate(0px, 0px)", "left":"0px", "top":"0px"});
    curr_module.insertAfter(current_line_segment);
  }else{
    load_toolbox();
    var curr_module = curr_module.detach();
    //curr_module.css({"left":e.pageX, "top":e.pageY});
    $("#overlay").append(curr_module);
  }
  event.preventDefault();
});

$('.draggable').resizable();
$('.draggable').draggable({cancel : '.draggable > * '});


$(document).on("click", ".deck_img_main",function(){
    //add it to the current slide
    current_deck = $(".deck-status-current").html();
    change_slide_target(current_deck);
    var deck_image_to_add =  $(this).clone();
    var deck_img_passover = $(deck_image_to_add.html());
    //deck_image_to_add.removeClass('deck_img_main');
    //var deck_image_to_add_container = $("<div></div>");
    //deck_image_to_add_container.append(deck_image_to_add);
    //deck_image_to_add.addClass("deck_img_container");
    deck_target.append(deck_img_passover);
    //deck_image_to_add.draggable();
    //alert(deck_image_to_add.html());
    deck_img_passover.resizable();
    deck_img_passover.parent().draggable();
});
