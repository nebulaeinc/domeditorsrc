var states = {
    waitingForNextWord: 0,
    MOD: 1,
    ADMIN: 2
};

var initialised = false;

//Load Initial document
var load_document = function load_document () {

  //ajax call to populate buffer
  //load_buffer i.e  @load_buffer

  //This is how we do
  for(var i = 0; i < buffer.length; i++){
    //waitForFirstItem ==  to see if we are in new segment and to update the queue called segmentPositions
    //waitingForNextWord ==  if a segment creating char is clicked -- if this val is true will not create new word
    //                    --> basically to see if previous was charachter or space
    // segStyleInit == flag to allow for multiple null values to be stacked next to eachother

    var val = buffer[i];
    new_line_logic(val);

    //depending on type of value in buffer[i] append to editor
    if(val == null){
      //do stylebuffer Logic
      styleIncomming(i);
    }
    else if(val == ' '){
      if(waitingForNextWord == false){
         new_line_segment();
         current_line_segment.append("&nbsp;");
         waitingForNextWord = true;
         segmentPositions.push(i);
      }
      else{
        current_line_segment.append("&nbsp;");
        if(waitForFirstItem == true){
        segmentPositions.push(i);
        waitForFirstItem = false; // this flag is for new para logic
        }
      }
      segStyleInit = true;
    }
    else{
      if(waitingForNextWord == true){     // ( current_line_segment == null || waitingForNextWord == true){}
        new_line_segment();
        segmentPositions.push(i);
        waitingForNextWord = false;
      }
      current_line_segment.append(buffer[i]);
      if(waitForFirstItem == true){
        segmentPositions.push(i);
        waitForFirstItem = false;
      }
      segStyleInit = true;
    }

  }
  //this is bad need a workaround for looping every time
  //some way to switch from segment to segment
  //nothing comes for free
  //splitEndOfStyle();
  //alert("allSegments: " + allSegments.length);
  //alert("segmentPositions: " + segmentPositions.length);
}


// what if null inbetween space and space or letter and letter
var splitEndOfStyle = function splitEndOfStyle(){
    var segPos = null;
    var segStart = null;
    var bufferRemaining = 0;
    var bufferMidIndex  = null;

    //create the array of jQuery objects
    var allSegments = [];
    $(".line-segment").each(function() {
      allSegments.push($(this));
    });


    for(var i = 0; i < endPosOfStyleQueue.length; i++){ // each is a split provided it meets the requirements
      //start if else chain
      var pos = endPosOfStyleQueue[i]; // the start pos of the segment
      //var allSegments = document.getElementsByClassName("line-segment");
      if(buffer[pos] == buffer[pos + 1] && buffer[pos] == ' '){ // if space or space
        //do the spliting operating
        var index = getMatchingSegment(pos); // now we have the segment we want to split segmentPositions[index]

        current_line_segment = allSegments[index]; // until method is over this is the line segment we will be working with
        current_line = current_line_segment.parent();
        segStart = segmentPositions[index];
        bufferMidIndex = pos - segStart;

        if(index == allSegments.length - 1){
          var continueLoop = true;
          var currentIndex = 0;
          while(continueLoop){ // to deal with edge case
            currentIndex = segStart + bufferMidIndex + bufferRemaining;
            if(styleBuffer[currentIndex] == null && buffer[currentIndex] == null){
              continueLoop = false;
            }else{
              bufferRemaining += 1;
            }
          }
        }else{
          bufferRemaining = segmentPositions[index + 1] - segmentPositions[index] - bufferMidIndex; //what if edge case ??
        }
        //Now to do the actual splitting 1 for loop for each half
        if(bufferMidIndex > 0){
          splitSegment(bufferMidIndex, segStart, pos, bufferRemaining);
          //because you split a two spaces
          //remember the load_document function has finished running
          endPosOfStyleQueue.shift();
          //segmentPositions.push(i);
        }
      }else if(buffer[pos] != null && buffer[pos + 1] != null && buffer[pos] != ' ' && buffer[pos + 1] != null){
        // so not null and neither is space: therefore letter and letter
        var pos = endPosOfStyleQueue[i];
        var index = getMatchingSegment(pos); // now we have the segment we want to split segmentPositions[index]
        current_line_segment = allSegments[index]; // until method is over this is the line segment we will be working with
        current_line = current_line_segment.parent();
        segStart = segmentPositions[index]; //start position of each
        bufferMidIndex = pos - segStart;

        if(index == allSegments.length - 1){
          var continueLoop = true;
          var currentIndex = 0;
          while(continueLoop){ // to deal with edge case
            currentIndex = segStart + bufferMidIndex + bufferRemaining;
            if(styleBuffer[currentIndex] == null && buffer[currentIndex] == null){
              continueLoop = false;
            }else{
              bufferRemaining += 1;
            }
          }
        }else{
          bufferRemaining = segmentPositions[index + 1] - segmentPositions[index] - bufferMidIndex; //what if edge case ??
        }
        if(bufferMidIndex > 0){
          splitSegment(bufferMidIndex, segStart, pos, bufferRemaining);
          //because you split a word
          endPosOfStyleQueue.shift(); //does it matter what order these are done in ?
          //segmentPositions.push(i);
        }
      }else{
        //do nothing
      }
    }
}


var splitSegment = function splitSegment (bufferMidIndex, segStart, pos, bufferRemaining) {
// could use the substring method here but it would not be mirroring the underlying data structures
  var tempSeg1 = $("<span class='line-segment'></span>");
  var tempSeg2 = $("<span class='line-segment'></span>");

  //move_multiple_back(current_segment_index);

  //var segmentText = current_line_segment.text().split('');
  for(var i = 0; i < bufferMidIndex; i++){
    tempSeg1.append(buffer[segStart + i]);
  }
  for(var i = 0; i < bufferRemaining; i++){
    tempSeg2.append(buffer[pos + i]);
  }

  var segIndex = current_line_segment.index();

  if(segIndex > current_line.children().length){
    current_line.append(tempSeg1);
    current_line.append(tempSeg2);
  }else if(segIndex < 1){
    current_line.prepend(tempSeg2);
    current_line.prepend(tempSeg1);
  }else{
    current_line.children().eq(segIndex).after(tempSeg1);
    tempSeg2.insertAfter(tempSeg1);
  }

  current_line_segment.remove();
  current_line_segment = tempSeg1;
  current_segment_index = tempSeg1.length;
  //@test
}

var splitSegments = function splitSegment (bufferMidIndex, segStart, pos, bufferRemaining) {
  //buffer and styleBuffer do not need to be modified as these are endings
  //1) remove the segment from dom and replace with two new segments
  //2) update segmentPositions array which stores the beggining of each segment

  // could use the substring method here but it would not be mirroring the underlying data structures
  var tempSeg1 = $("<span class='line-segment'></span>");
  var tempSeg2 = $("<span class='line-segment'></span>");

  for(var x = 0; x < bufferMidIndex; x++){
    tempSeg1.append(buffer[segStart + x]);
  }
  for(var x = 0; x < bufferRemaining; x++){
    tempSeg2.append(buffer[pos + x]);
  }
  //stage 1
  var segIndex = current_line_segment.index();

  current_line_segment.remove();
  if(segIndex > current_line.children().length){
    current_line.append(tempSeg1);
    current_line.append(tempSeg2);
  }else if(segIndex < 1){
    current_line.prepend(tempSeg2);
    current_line.prepend(tempSeg1);
  }else{
    current_line.children().eq(segIndex -1).after(tempSeg1);
    current_line.children().eq(segIndex).after(tempSeg2);
  }
  //tempSeg2.css({'color':'red'});

  //stage 2
  var segPosIndex = segmentPositions.indexOf(segStart); // the position of segment in segment positions NB!:: does not account for empty segments
  segmentPositions.slice(segPosIndex,1, tempSeg2);
  segmentPositions.slice(segPosIndex,0, tempSeg1);
}

//This method takes in i which is the start position of the segment that you want to find
// and returns itt which is the position of the segment in segmentPositions[] and allSegments[]
var getMatchingSegment = function getMatchingSegment (i){
  for(var itt = 0; itt < segmentPositions.length; itt++){
    if(itt != segmentPositions.length -1){ // you are not at the end

      if(segmentPositions[itt] == segmentPositions[itt+1]){
        if(i == segmentPositions[itt]){
          return itt;
        }
      }
      else if(i >= segmentPositions[itt] && i < segmentPositions[itt+1]){
          return itt;
      }
    }else{
       return itt; // you may be wrong but at least you returned something
    }
  }
}


var new_line_logic = function new_line_logic (val) {

    if(current_para != null && current_line != null && current_line_segment != null && val != null){
      if(isLineFull(val)){
        if(val == " "){ //end of previous word
          //create new line
          new_line();
          waitingForNextWord = false;
          //new_line_segment();
        }else if(waitingForNextWord == true){ // currently wating for new word
          new_line();
          //new_line_segment();
        }else if(current_line_segment.text().length > 0){ //in the middle of current segment
            if(current_line_segment.text().charAt(0) != ' '){
              current_line_segment.remove();
              new_line();
              current_line.append(current_line_segment);
            }
        }else if(val == null){
          //do nothing
        }
      }
    }
 }


var styleIncomming = function styleIncomming (index) {
  var val = styleBuffer[index];
  var styleS = null;
  var prefix = null;
  try{
    if(styleBuffer[index] != null){
      styleS = styleBuffer[index];
      prefix = styleS.substring(0,2);
    }
  }catch(err){
    console.log("regex is less than two chars length");
  }


  switch(prefix){
    case "pa":
        new_paragraph();
        if(initialised == true){
          if(segStyleInit == true){ // a new line segment needs to be made
            new_line();
            new_line_segment();
          }else{ // theres a line segment to take that was stacked before this para
            current_line_segment.remove();
            new_line();
            current_line.append(current_line_segment);
          }
        }else{
          new_line();
          new_line_segment();
          initialised = true;
          segmentPositions.push(index);
        }
        waitForFirstItem = true; // this flag waits until a letter or space is added to the segment to update it
        segStyleInit = false;

        break;
    case "/#":
          if(segStyleInit == true){ //if first in line of null styles
            new_line_segment();
            waitingForNextWord = false;
            segStyleInit = false;
          }
          endPosOfStyleQueue.push(parseInt(styleS.split(/\|/g)[1]));
          //endPosOfStyleQueue.push(/\#(.*)\|/g.exec(styleS)[2]);
          colorQueue.push(styleBuffer[index]);
        break;
    case "/s":
        if(segStyleInit == true){ //if first in line of null styles
          new_line_segment();
          waitingForNextWord = false;
          segStyleInit = false;
        }
        endPosOfStyleQueue.push(parseInt(styleS.split(/\|/g)[1]));
        //endPosOfStyleQueue.push(/\#(.*)\|/g.exec(styleS)[2]);
        sizeQueue.push(styleBuffer[index]);
        break;
    case "/f":
        if(segStyleInit == true){ //if first in line of null styles
          new_line_segment();
          waitingForNextWord = false;
          segStyleInit = false;
        }
        endPosOfStyleQueue.push(parseInt(styleS.split(/\|/g)[1]));
        //endPosOfStyleQueue.push(/\#(.*)\|/g.exec(styleS)[2]);
        fontQueue.push(styleBuffer[index]);
        break;
    default:
        console.log("Check styleIncomming");
  }
}

//adds new line segment after current line segment
var new_line_segment = function new_line_segment(){
   if(current_line == null){
    new_line();
   }
   var new_line_segment = $("<span class='line-segment'></span>");
   current_line.append(new_line_segment);
   previous_line_segment = current_line_segment;
   current_line_segment =  new_line_segment;
   numOfSegments += 1;
}

//adds new line after current line
var new_line = function new_line () {
  console.log("Um what ?!");
  if(current_para == null){
    new_paragraph();
  }
  var line  =  $("<span class='line'></span>");
  current_para.append(line);
  previous_line = current_line;
  current_line = line;
}

//adds new paragraph after current paragraph
var new_paragraph = function new_paragraph (){
  var new_para = $("<div class='para'></div>");
  if(current_para == null){
    $('#firstPage').append(new_para);
  }
  else{
    new_para.insertAfter(current_para);
  }
  previous_para = current_para;
  current_para = new_para;

  //previous_line = current_line;
  //previous_line_segment = current_line_segment;
  //current_line = null;
  //current_line_segment = null;
}
