function textFont($text){
    document.execCommand('fontName',false,$text);
  };

function textSize($size){
    document.execCommand('fontSize',false,$size);
  };

function twitterChoice($option){
    alert($option);
  };


$(document).ready(function(){
    var sheet = document.styleSheets[0];


    $('#underLineText').on('click', function(){
      document.execCommand('underline');
    });

    $('#boldText').click(function() {
      document.execCommand('bold');
    });

    $('#italicsText').click(function(){
      document.execCommand('italic');
    });

    $('#strikeText').click(function(){
      document.execCommand('strikeThrough');
    });
    $('#fontColourText').click(function(){
      document.execCommand('foreColor',false,'brown');
    });
    $('#reloadScript').click(function(){
      reload_js("/home/mr/extra/modules/app.js");
    });

    $('#fileBtn').click(function(){
      document.getElementById("fileDropDown").classList.toggle("show");
    });
    $('#editBtn').click(function(){
      document.getElementById("editDropDown").classList.toggle("show");
    });
    $('#viewBtn').click(function(){
      document.getElementById("viewDropDown").classList.toggle("show");
    });
    $('#insertBtn').click(function(){
      document.getElementById("insertDropDown").classList.toggle("show");
    });
    $('#formatBtn').click(function(){
      document.getElementById("formatDropDown").classList.toggle("show");
    });
    $('#stylesBtn').click(function(){
      document.getElementById("stylesDropDown").classList.toggle("show");
    });
    $('#helpBtn').click(function(){
      document.getElementById("helpDropDown").classList.toggle("show");
    });


    $('#twitterBtn').click(function(){
      document.getElementById("twitterDropDown").classList.toggle("show");

    });


    window.onclick = function(event) {
      if (!event.target.matches('.dropbtn')) {

        var dropdowns = document.getElementsByClassName("dropdown-content");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
          var openDropdown = dropdowns[i];
          if (openDropdown.classList.contains('show')) {
            openDropdown.classList.remove('show');
          }
        }
      }
    }

    function getSelection(){
      var text = "";
          if (window.getSelection) {
              text = window.getSelection().toString();
          } else if (document.selection && document.selection.type != "Control") {
              text = document.selection.createRange().text;
          }
        return text;
    }
});

var load_toolbox = function load_toolbox(toolbox_name){
  $.ajax({
        type: "GET",
        cache: false,
        data:{
          'name': selectedmod,
          'postid': postid
        },
        url: "/toolbox/",
         success: function(data) {
           $("#toolbox-container-main").html(data);
           //reset_color();
         }
     });
}

var load_side_panel = function load_side_panel(){
  $.ajax({
        type: "GET",
        cache: false,
        dataType: "json",
        data:{
          'name': selectedmod,
          'postid': postid
        },
        url: "/toolbox/toolboxPack",
         success: function(data) {
           if(data.css != ""){
             alert("isa empty!");
           }
           $("#toolbox-container-main").html(data);
           $("#side-panel-main").css({"visibility":"visible"});
           //reset_color();
         }
     });
}

var load_toolbox_js = function load_toolbox_js(){
  $.ajax({
        type: "GET",
        cache: false,
        data:{
          'name': selectedmod,
          'postid': postid
        },
        url: "/toolbox/getjs",
         success: function(data) {
           $('body').append(data);
           $('#module-options').css({"visibility":"hidden"});
              var final_idea = $("<script src='http://res.cloudinary.com/dhasw4cz8/raw/upload/v1490260745/ppstart_cqsuvw.js'></script>");
              $('body').append(final_idea);

           /*
           $(function() {
             $.deck('.slide');
           });

           var current_deck = "#deck-slide-1";
           var num_slides = "3";
           $(document).on('click', '#deck-add-text', function(){
               $(current_deck).append("<div class='mov-deck-obj' contenteditable='true'>placeholder</div>");
           });

           $(document).on('click', '#deck-add-image', function(){
               $(current_deck).append("<div class='mov-deck-obj' contenteditable='true'>placeholder</div>");
           }); */
         }
     });
}

var reset_color = function reset_color(){
  $("#full").spectrum({
      color: "#ECC",
      showInput: true,
      className: "full-spectrum",
      showInitial: true,
      showPalette: true,
      showSelectionPalette: true,
      maxSelectionSize: 10,
      preferredFormat: "hex",
      localStorageKey: "spectrum.demo",
      move: function (color) {

      },
      show: function () {

      },
      beforeShow: function () {

      },
      hide: function () {

      },
      change: function() {

      },
      palette: [
          ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
          "rgb(204, 204, 204)", "rgb(217, 217, 217)","rgb(255, 255, 255)"],
          ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
          "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
          ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
          "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
          "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
          "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
          "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
          "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
          "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
          "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
          "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
          "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
      ]
  });
}

var loaded_modules = [];

var load_toolbox_complete = function load_toolbox_complete(){
  $.ajax({
        type: "GET",
        cache: false,
        dataType: "json",
        data:{
          'name': selectedmod,
          'postid': postid
        },
        url: "/toolbox/toolboxPack",
         success: function(data) {
           if(loaded_modules.indexOf(selectedmod) == -1){
             if(data.css != ""){
               $('head').append(data.css);
             }
           }
           if(data.html != ""){
              $("#toolbox-container-main").html(data.html);
           }
           if(data.htmlside != ""){
              $("#side-panel-main").css({"visibility":"visible"});
              $("#side-panel-main").html(data.htmlside);
           }else{
             $("#side-panel-main").css({"visibility":"hidden"});
           }

            if(loaded_modules.indexOf(selectedmod) == -1){
              if(data.script != ""){
                $('head').append(data.script);
              }
            }
           //reset_color();
           loaded_modules.push(selectedmod);
         }
     });
}

//load css into a single placeholder in head of documen
