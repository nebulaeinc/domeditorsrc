var getTwitterStatusWriter  = function getTwitterStatusWriter(handledata) {
  return $.ajax({
        type: "GET",
        cache: false,
        data:{
          'name': name,
          'postid': postid
        },
        url: "/twitterapi/PostTweet",
         success: function(data) {
           handledata(data);
         }
     });
}

// "/twitterapi/createTimeline"
// "/twitterapi/PostTweet"

var module_options = function module_options(handledata) {
  return $.ajax({
        type: "GET",
        url: "/choosem/choosem",
         success: function(data) {
           handledata(data);
         }
     });
}


var module_selection = function module_selection(handledata) {
  return $.ajax({
        type: "GET",
        cache: false,
        data:{
          'name': name,
        },
        url: "/choosem/modselec",
         success: function(data) {
           handledata(data);
         }
     });
}

var module_selection_2 = function module_selection(handledata) {
  return $.ajax({
        type: "GET",
        cache: false,
        data:{
          'name': name,
          'postid': postid,
          'mod_id' : "",
          'method': selectedmod,
        },
        url: "/choosem/router",
         success: function(data) {
           handledata(data);
         }
     });
}

var selection_level = 0;
var freemodules = [];
var freemodulesitt = 293;
var targeted_module = "";
var current_deck = $(".deck-status-current");
var deck_target = $("#deck-slide-1");
var num_slides = "3";

//Therefore no only serve an empty module container system but also let
//the view.method create an instance with a unique id

//the module has an class which correlates to the toolbox and function.js and html
//the module has an id which gives it unique identification
//the id correlates to a space in mongo where the underlying data of the module is stored
        //i.e if graph ==> dat-set && options document
        //i.e if presentation ==> data-set the different divs
        //i.e if excel table ==> data-set of the table data + formulas at specific locations
//The positioning and styling of the document is stored in a json file
//The Json file is broken into page -- para -- line[] -- line-segment []
  //each of these have a sub-document broken down into all available css attributes that are added to the
  //element sequentially at client side compilation time
$(document).on('click', '.single-list-2', function(){
  var id = $(this).attr('id');
  name = id;
  if(name == "bargraph" ){
    $("#module-options-list").css({'visibility':'hidden'});
    selectedmod = "bargraph";
    module_selection_2(function(output){
      // generate an id to manage module
      var temp_module_id = generate_mod_id();
      // use the returned html data to populate new module
      dynamic_module_loader_over(output,temp_module_id);
      //this is where the layout engine should focus
      // all modules that dynamically load will access targeted_module and
      // do work on that id
      targeted_module = temp_module_id;
      //load the load_toolbox_complete func to add functionality
      load_toolbox_complete();
      $('.draggable').draggable().resizable();
      refocus("myChart");
    });
  }
});


$(document).on('click', '.single-list', function(){
  var id = $(this).attr('id')
  name = id;
  if(name == "Spreadsheet"){
    $("#module-options-list").css({'visibility':'hidden'});
    selectedmod = name;
    var tmp_id = embedded_module_custom();
    initialise_handson(tmp_id);
    $('.draggable').draggable({cancel : '.draggable > * '}).resizable({handles:'se'});
  }else if(name == "Presentation"){
    $("#module-options-list").css({'visibility':'hidden'});
    selectedmod = "deck";
    module_selection_2(function(output){
      /*
      selection_level = 0;
      var temp_module_id = 'fm-' + freemodulesitt;
      styleBuffer[gap_start] = temp_module_id;
      var temp_module = null;
      var deckspace = $("<span class='line'></span>");
      var deckinsertion = $("<div border-color='red' id='"+temp_module_id+"' class=' module line-segment draggable "+selectedmod+"'><div id='prezi-mover'></div>"+output+"</div>");
      deckspace.append(deckinsertion);
      current_para.append(deckspace);
      freemodules[freemodulesitt] = temp_module;
      freemodulesitt += 1;
      deckinsertion.css({"width":"500px"});
      deckinsertion.css({"height":"500px"})
      load_toolbox_js();
      //$(function() {
        //$.deck('.slide');
      //});
      var side_panel = $("<div id='side-panel-inner'>"+
        "<div id='toolbox-deck'>"+
        "<button id='deck-add-text' class='deck-tool-item'>Text</button>"+
        "<button id='deck-add-img' class='deck-tool-ite'>Img</button>"+
        "<button id='add-video' class='deck-tool-item'>Vid</button>"+
        "<button id='add-video' class='deck-tool-item'>Deck</button>"+
        "<div> <div id='toolbox-deck-preview'> </div> </div>");
      $('body').append(side_panel); */
      var side_panel_deck = "<div id='side-panel-inner'>"+
                            "<div id='toolbox-deck'>"+
                              "<button id='deck-add-text' class='deck-tool-item'>Text</button>"+
                              "<button id='deck-add-img' class='deck-tool-item'>Img</button>"+
                              "<button id='add-video' class='deck-tool-item'>Vid</button>"+
                              "<button id='add-video' class='deck-tool-item'>Deck</button>"+
                            "</div>"+
                            "<div id='deck_image_results'></div>"+
                            "</div>";
      $("#side-panel-main").html(side_panel_deck);



      output = "<div class='deck-container'>"+
      "<section id='deck-slide-1' class='slide'>"+
        "<div class='mov-deck-obj'  contenteditable='true'>Slide 1</div>"+
      "</section>"+
      "<section id='deck-slide-2' class='slide'>"+
        "<div class='mov-deck-obj'  contenteditable='true'>Slide 2</div>"+
      "</section>"+
      "<section id='deck-slide-3' class='slide'>"+
        "<div class='mov-deck-obj'  contenteditable='true'>Slide 3</div>"+
      "</section>"+
      "<div aria-role='navigation'>"+
        "<a href='#' class='deck-prev-link' title='Previous'>&#8592;</a>"+
        "<a href='#' class='deck-next-link' title='Next'>&#8594;</a>"+
      "</div>"+
      "<p class='deck-status' aria-role='status'>"+
        "<span class='deck-status-current'>"+"</span>"+
        "<span class='deck-status-total'>"+"</span>"+
      "</p>"+
      "<form action='.' method='get' class='goto-form'>"+
        "<label for='goto-slide'>Go to slide:</label>"+
        "<input type='text' name='slidenum' id='goto-slide' list='goto-datalist'>"+
        "<datalist id='goto-datalist'></datalist>"+
        "<input type='submit' value='Go'>"+
      "</form>"+
    "</div>";

      var temp_module_id = generate_mod_id();
      dynamic_module_loader_over(output,temp_module_id);
      targeted_module = temp_module_id;
      //load_toolbox_complete();
      //var new_div = $("<div id='hihi' class='draggable'></div>");
      //$("body").append(new_div);
      //new_div.html(output);
      //new_div.css({"position":"absolute", "width":"500px", "height":"500px", "z-index":"100000000000000 !important"});
      //$('draggable').draggable().resizable();


      var quick_action = "<link rel='stylesheet' media='screen' href='http://res.cloudinary.com/dhasw4cz8/raw/upload/v1490582515/deck.core_qumnh7.css'>"+
      "<link rel='stylesheet' media='screen' href='http://res.cloudinary.com/dhasw4cz8/raw/upload/v1490582515/deck.goto_mgns0v.css'>"+
      "<link rel='stylesheet' media='screen' href='http://res.cloudinary.com/dhasw4cz8/raw/upload/v1490582515/deck.menu_jm0iyz.css'>"+
      "<link rel='stylesheet' media='screen' href='http://res.cloudinary.com/dhasw4cz8/raw/upload/v1490582515/deck.navigation_dzv1yu.css'>"+
      "<link rel='stylesheet' media='screen' href='http://res.cloudinary.com/dhasw4cz8/raw/upload/v1490582515/deck.status_t7ytgm.css'>"+
      "<link rel='stylesheet' media='screen' href='http://res.cloudinary.com/dhasw4cz8/raw/upload/v1490582515/deck.scale_xhei8p.css'>"+
      "<link rel='stylesheet' media='screen' href='http://res.cloudinary.com/dhasw4cz8/raw/upload/v1490582514/web-2.0_r36oxa.css'>"+
      "<link rel='stylesheet' media='screen' href='http://res.cloudinary.com/dhasw4cz8/raw/upload/v1490582515/horizontal-slide_tzatpd.css'>"+
      "<link rel='stylesheet' media='print' href='http://res.cloudinary.com/dhasw4cz8/raw/upload/v1490582515/print_mzs14x.css'>"+
      "<script text/javascript src='http://res.cloudinary.com/dhasw4cz8/raw/upload/v1490586294/deck_final_wipqvy.js'>"+"</script>";
      //"<script text/javascript src='http://res.cloudinary.com/dhasw4cz8/raw/upload/v1490582515/jq_cko1wb.js'>"+"</script>";
      $("head").append(quick_action);

      /*
      quick_action = "<script text/javascript src='http://res.cloudinary.com/dhasw4cz8/raw/upload/v1490582515/deck.core_rouwsu.js'>"+"</script>";
      $("head").append(quick_action);
      quick_action = "<script text/javascript src='http://res.cloudinary.com/dhasw4cz8/raw/upload/v1490582515/deck.menu_esd3g2.js'>"+"</script>"+
      "<script text/javascript src='http://res.cloudinary.com/dhasw4cz8/raw/upload/v1490582515/deck.goto_esdwdh.js'>"+"</script>"+
      "<script text/javascript src='http://res.cloudinary.com/dhasw4cz8/raw/upload/v1490582515/deck.status_xxhqfs.js'>"+"</script>"+
      "<script text/javascript src='http://res.cloudinary.com/dhasw4cz8/raw/upload/v1490582515/deck.navigation_eqnddf.js'>"+"</script>"+
      "<script text/javascript src='http://res.cloudinary.com/dhasw4cz8/raw/upload/v1490582515/deck.scale_gyvciz.js'>"+"</script>"; */
      /*var script = document.createElement( 'script' );
      script.type = 'text/javascript';
      //script.src = "http://res.cloudinary.com/dhasw4cz8/raw/upload/v1490573363/launch_deck_fyqdx7.js";
      script.src = "http://res.cloudinary.com/dhasw4cz8/raw/upload/v1490583257/deck_launch_npczx8.js";
      $("#prezi-holder").append( script ); */
      $('.draggable').draggable({cancel : '.draggable > * '}).resizable();
    });
  }else if(name == "Youtube"){
    // calls the youtubejs module in to insert into document
    $("#module-options-list").css({'visibility':'hidden'});
    selectedmod = "Youtube";
    module_selection_2(function(output){
      // generate an id to manage module
      var temp_module_id = generate_mod_id();
      // use the returned html data to populate new module
      dynamic_module_loader_over(output,temp_module_id);
      //this is where the layout engine should focus
      // all modules that dynamically load will access targeted_module and
      // do work on that id
      targeted_module = temp_module_id;
      //load the load_toolbox_complete func to add functionality
      load_toolbox_complete();
      $('.draggable').draggable().resizable();
    });
  }else{
  module_selection(function(output){
    $("#module-options-list").html(output);
  });
  }
});


// this array stores the free floating modules
//  module_id css and javascript pointers


var initialise_handson = function initialise_handson(tmp_id) {
  var dataObject = [
   {id: 1, flag: 'EUR', currencyCode: 'EUR', currency: 'Euro',	level: 0.9033, units: 'EUR / USD', asOf: '08/19/2015', onedChng: 0.0026},
   {id: 2, flag: 'JPY', currencyCode: 'JPY', currency: 'Japanese Yen', level: 124.3870, units: 'JPY / USD', asOf: '08/19/2015', onedChng: 0.0001},
   {id: 3, flag: 'GBP', currencyCode: 'GBP', currency: 'Pound Sterling', level: 0.6396, units: 'GBP / USD', asOf: '08/19/2015', onedChng: 0.00},
   {id: 4, flag: 'CHF', currencyCode: 'CHF', currency: 'Swiss Franc',	level: 0.9775, units: 'CHF / USD', asOf: '08/19/2015', onedChng: 0.0008},
   {id: 5, flag: 'CAD', currencyCode: 'CAD', currency: 'Canadian Dollar',	level: 1.3097, units: 'CAD / USD', asOf: '08/19/2015', onedChng: -0.0005},
   {id: 6, flag: 'AUD', currencyCode: 'AUD', currency: 'Australian Dollar',	level: 1.3589, units: 'AUD / USD', asOf: '08/19/2015', onedChng: 0.0020},
   {id: 7, flag: 'NZD', currencyCode: 'NZD', currency: 'New Zealand Dollar',	level: 1.5218, units: 'NZD / USD', asOf: '08/19/2015', onedChng: -0.0036},
   {id: 8, flag: 'SEK', currencyCode: 'SEK', currency: 'Swedish Krona',	level: 8.5280, units: 'SEK / USD', asOf: '08/19/2015', onedChng: 0.0016},
   {id: 9, flag: 'NOK', currencyCode: 'NOK', currency: 'Norwegian Krone',	level: 8.2433, units: 'NOK / USD', asOf: '08/19/2015', onedChng: 0.0008},
   {id: 10, flag: 'BRL', currencyCode: 'BRL', currency: 'Brazilian Real',	level: 3.4806, units: 'BRL / USD', asOf: '08/19/2015', onedChng: -0.0009},
   {id: 11, flag: 'CNY', currencyCode: 'CNY', currency: 'Chinese Yuan',	level: 6.3961, units: 'CNY / USD', asOf: '08/19/2015', onedChng: 0.0004},
   {id: 12, flag: 'RUB', currencyCode: 'RUB', currency: 'Russian Rouble',	level: 65.5980, units: 'RUB / USD', asOf: '08/19/2015', onedChng: 0.0059},
   {id: 13, flag: 'INR', currencyCode: 'INR', currency: 'Indian Rupee',	level: 65.3724, units: 'INR / USD', asOf: '08/19/2015', onedChng: 0.0026},
   {id: 14, flag: 'TRY', currencyCode: 'TRY', currency: 'New Turkish Lira',	level: 2.8689, units: 'TRY / USD', asOf: '08/19/2015', onedChng: 0.0092},
   {id: 15, flag: 'THB', currencyCode: 'THB', currency: 'Thai Baht',	level: 35.5029, units: 'THB / USD', asOf: '08/19/2015', onedChng: 0.0044},
   {id: 16, flag: 'IDR', currencyCode: 'IDR', currency: 'Indonesian Rupiah',	level: 13.83, units: 'IDR / USD', asOf: '08/19/2015', onedChng: -0.0009},
   {id: 17, flag: 'MYR', currencyCode: 'MYR', currency: 'Malaysian Ringgit',	level: 4.0949, units: 'MYR / USD', asOf: '08/19/2015', onedChng: 0.0010},
   {id: 18, flag: 'MXN', currencyCode: 'MXN', currency: 'Mexican New Peso',	level: 16.4309, units: 'MXN / USD', asOf: '08/19/2015', onedChng: 0.0017},
   {id: 19, flag: 'ARS', currencyCode: 'ARS', currency: 'Argentinian Peso',	level: 9.2534, units: 'ARS / USD', asOf: '08/19/2015', onedChng: 0.0011},
   {id: 20, flag: 'DKK', currencyCode: 'DKK', currency: 'Danish Krone',	level: 6.7417, units: 'DKK / USD', asOf: '08/19/2015', onedChng: 0.0025},
   {id: 21, flag: 'ILS', currencyCode: 'ILS', currency: 'Israeli New Sheqel',	level: 3.8262, units: 'ILS / USD', asOf: '08/19/2015', onedChng: 0.0084},
   {id: 22, flag: 'PHP', currencyCode: 'PHP', currency: 'Philippine Peso',	level: 46.3108, units: 'PHP / USD', asOf: '08/19/2015', onedChng: 0.0012}
 ];
 var currencyCodes = ['EUR', 'JPY', 'GBP', 'CHF', 'CAD', 'AUD', 'NZD', 'SEK', 'NOK', 'BRL', 'CNY', 'RUB', 'INR', 'TRY', 'THB', 'IDR', 'MYR', 'MXN', 'ARS', 'DKK', 'ILS', 'PHP'];

 var flagRenderer = function(instance, td, row, col, prop, value, cellProperties) {
   var currencyCode = value;

   while (td.firstChild){
     td.removeChild(td.firstChild);
   }
   if (currencyCodes.indexOf(currencyCode) > -1) {
     var flagElement = document.createElement('DIV');
     flagElement.className = 'flag ' + currencyCode.toLowerCase();
     td.appendChild(flagElement);
   } else {
     var textNode = document.createTextNode(value === null ? '' : value);
     td.appendChild(textNode);
   }
 };

 var hotElement = document.querySelector('#sample');
 var hotElementContainer = hotElement.parentNode;
 var hotSettings = {
   data: dataObject,
   columns: [
       {
           data: 'id',
           type: 'numeric',
           width: 20
       },
       {
           data: 'flag',
     renderer: flagRenderer
       },
       {
           data: 'currencyCode',
           type: 'text'
       },
       {
           data: 'currency',
           type: 'text'
       },
       {
           data: 'level',
           type: 'numeric',
           format: '0.0000'
       },
       {
           data: 'units',
           type: 'text'
       },
       {
           data: 'asOf',
           type: 'date',
           dateFormat: 'MM/DD/YYYY'
       },
       {
           data: 'onedChng',
           type: 'numeric',
           format: '0.00%'
       }
   ],
   stretchH: 'all',
   width: 1000,
   autoWrapRow: true,
   height: 1000,
   maxRows: 22,
   rowHeaders: true,
   contextMenu: true,
   colHeaders: [
       'ID',
       'Country',
       'Code',
       'Currency',
       'Level',
       'Units',
       'Date',
       'Change'
   ]
};

 var hot = new Handsontable(hotElement, hotSettings);
}

var embedded_module_custom = function embedded_module_custom(){
  selection_level = 0;
  $("#module-options-list").css({'visibility':'hidden'});
  var temp_module_id = 'fm-' + freemodulesitt;
  styleBuffer[gap_start] = temp_module_id;
  var temp_module = null;
  temp_module = "<div border-color='red' id='"+temp_module_id+"' class='line-segment draggable ui-widget-content module "+selectedmod+"'><div id='mod-inner-container'><div id='sample'></div></div></div>";
  freemodules[freemodulesitt] = temp_module;
  freemodulesitt += 1;
  //temp_module = "<div class='resizable draggable line-segment module'><div border-color='red' class=''><span id='"+temp_module_id+"'  class='module' color='brown'>"+ output +"</span></div><div class='ui-resizable-handle ui-resizable-nw'></div><div class='ui-resizable-handle ui-resizable-ne'></div>   <div class='ui-resizable-handle ui-resizable-sw'></div>        <div class='ui-resizable-handle ui-resizable-se'></div>        <div class='ui-resizable-handle ui-resizable-n'></div>        <div class='ui-resizable-handle ui-resizable-s'></div>        <div class='ui-resizable-handle ui-resizable-e'></div><div class='ui-resizable-handle ui-resizable-w'></div> </div>";
  //insert the module
  $(temp_module).insertAfter(current_line_segment);
  current_line_segment = current_line_segment.next();
  current_line_segment.css({"border":"1px solid blue"});
  //insert  a blank segment so user can continue writing
  var ghost_seg_mod = $("<span  class='line-segment'></span>");
  $(ghost_seg_mod).insertAfter(current_line_segment);
  current_line_segment = ghost_seg_mod;
  current_line_segment.css({"background-color":"pink"});
  //update cycle
  current_segment_index = 0; //ghost span starts at -1
  update_data_cycle()
  //sort out data structure
  gap_start += 1;
  //--------------- Cursor -------------------------------
  //get width of module
  var mod_width =   $("#"+ temp_module_id).outerWidth();
  var move_rigth_px = mod_width + $(".cursor").offset().left;
  $(".cursor").css({"left": move_rigth_px+"px"});
  return temp_module_id;
}


//outstanding
var embedded_module = function embedded_module(){
  module_selection_2(function(output){
    //html && runable js come with the div
    //toolbox calls toolbox method to the re-insert if of different id

    selection_level = 0;
    $("#module-options-list").css({'visibility':'hidden'});
    //method call to ajax code to get module  template
    var temp_module_id = 'fm-' + freemodulesitt;
    styleBuffer[gap_start] = temp_module_id;
    //var youtube  = "<iframe width='300' height='170' src='https://www.youtube.com/embed/e-Ho7IwupxI' frameborder='0' allowfullscreen></iframe>";
    // create the module
    //var temp_module = "<div border-color='red'   class='line-segment'><span id='"+temp_module_id+"'  class='module' color='brown'> Hello This is a test! <span id='tester1' color='red'>Check it</span> </span></div>";
    //var temp_module  = getTwitterStatusWriter();
    var temp_module = null;
    //temp_module = "<div border-color='red' id='"+temp_module_id+"'  class='line-segment module'><span class='module_layer' color='brown'>"+ youtube +"</span></div>";
    temp_module = "<div border-color='red' id='"+temp_module_id+"' class='line-segment module "+selectedmod+"'>"+output+"</div>";
    //temp_module = "<div class='resizable draggable line-segment module'><div border-color='red' class=''><span id='"+temp_module_id+"'  class='module' color='brown'>"+ output +"</span></div><div class='ui-resizable-handle ui-resizable-nw'></div><div class='ui-resizable-handle ui-resizable-ne'></div>   <div class='ui-resizable-handle ui-resizable-sw'></div>        <div class='ui-resizable-handle ui-resizable-se'></div>        <div class='ui-resizable-handle ui-resizable-n'></div>        <div class='ui-resizable-handle ui-resizable-s'></div>        <div class='ui-resizable-handle ui-resizable-e'></div><div class='ui-resizable-handle ui-resizable-w'></div> </div>";

    freemodules[freemodulesitt] = temp_module;
    freemodulesitt += 1;
    //temp_module = "<div class='resizable draggable line-segment module'><div border-color='red' class=''><span id='"+temp_module_id+"'  class='module' color='brown'>"+ output +"</span></div><div class='ui-resizable-handle ui-resizable-nw'></div><div class='ui-resizable-handle ui-resizable-ne'></div>   <div class='ui-resizable-handle ui-resizable-sw'></div>        <div class='ui-resizable-handle ui-resizable-se'></div>        <div class='ui-resizable-handle ui-resizable-n'></div>        <div class='ui-resizable-handle ui-resizable-s'></div>        <div class='ui-resizable-handle ui-resizable-e'></div><div class='ui-resizable-handle ui-resizable-w'></div> </div>";
    //insert the module
    $(temp_module).insertAfter(current_line_segment);
    current_line_segment = current_line_segment.next();
    current_line_segment.css({"border":"1px solid blue"});
    //insert  a blank segment so user can continue writing
    var ghost_seg_mod = $("<span  class='line-segment'></span>");
    $(ghost_seg_mod).insertAfter(current_line_segment);
    current_line_segment = ghost_seg_mod;
    current_line_segment.css({"background-color":"pink"});
    //update cycle
    current_segment_index = 0; //ghost span starts at -1
    update_data_cycle()
    //sort out data structure
    gap_start += 1;
    //--------------- Cursor -------------------------------
    //get width of module
    var mod_width =   $("#"+ temp_module_id).outerWidth();
    var move_rigth_px = mod_width + $(".cursor").offset().left;
    $(".cursor").css({"left": move_rigth_px+"px"});
  });
}

var free_module = function free_module(e){
  module_selection_2(function(output){
    selection_level = 0;
    $("#module-options-list").css({'visibility':'hidden'});
    var temp_module = null;
    temp_module = "<div border-color='red'  class='module'>"+output+"</div>";
    $("#overlay").append(temp_module);
  });
}

$('#module-options').on( 'mousewheel DOMMouseScroll', function (e) {

var e0 = e.originalEvent;
var delta = e0.wheelDelta || -e0.detail;

this.scrollTop += ( delta < 0 ? 1 : -1 ) * 30;
e.preventDefault();
});

var dynamic_module_loader_doc = function dynamic_module_loader_doc(moduledata) {
  selection_level = 0;
  $("#module-options-list").css({'visibility':'hidden'});
  //method call to ajax code to get module  template
  var temp_module_id = 'fm-' + freemodulesitt;
  styleBuffer[gap_start] = temp_module_id;
  var temp_module = null;
  temp_module = "<div border-color='red' id='"+temp_module_id+"' class='line-segment module "+selectedmod+"'>"+moduledata+"</div>";

  freemodules[freemodulesitt] = temp_module;
  freemodulesitt += 1;
  //temp_module = "<div class='resizable draggable line-segment module'><div border-color='red' class=''><span id='"+temp_module_id+"'  class='module' color='brown'>"+ output +"</span></div><div class='ui-resizable-handle ui-resizable-nw'></div><div class='ui-resizable-handle ui-resizable-ne'></div>   <div class='ui-resizable-handle ui-resizable-sw'></div>        <div class='ui-resizable-handle ui-resizable-se'></div>        <div class='ui-resizable-handle ui-resizable-n'></div>        <div class='ui-resizable-handle ui-resizable-s'></div>        <div class='ui-resizable-handle ui-resizable-e'></div><div class='ui-resizable-handle ui-resizable-w'></div> </div>";
  //insert the module
  $(temp_module).insertAfter(current_line_segment);
  current_line_segment = current_line_segment.next();
  current_line_segment.css({"border":"1px solid blue"});
  //insert  a blank segment so user can continue writing
  var ghost_seg_mod = $("<span  class='line-segment'></span>");
  $(ghost_seg_mod).insertAfter(current_line_segment);
  current_line_segment = ghost_seg_mod;
  current_line_segment.css({"background-color":"pink"});
  //update cycle
  current_segment_index = 0; //ghost span starts at -1
  update_data_cycle()
  //sort out data structure
  gap_start += 1;
  //--------------- Cursor -------------------------------
  //get width of module
  var mod_width =  $("#"+ temp_module_id).outerWidth();
  var move_rigth_px = mod_width + $(".cursor").offset().left;
  $(".cursor").css({"left": move_rigth_px+"px"});
}


var dynamic_module_loader_over = function dynamic_module_loader_over(moduledata,temp_module_id) {
  selection_level = 0;
  //method call to ajax code to get module  template
  temp_module = "<div border-color='red' id='"+temp_module_id+"' class='line-segment draggable module "+selectedmod+"'>"+moduledata+"</div>";
  //temp_module = "<div id='"+temp_module_id+"'>"+moduledata+"</div>";
  $("#overlay").append(temp_module);
}


var generate_mod_id = function generate_mod_id(){
  var temp_module_id = 'fm-' + freemodulesitt;
  var temp_module = null;
  freemodules[freemodulesitt] = temp_module;
  freemodulesitt += 1;
  return temp_module_id;
}
// file Management --- filestack
