// This logic will return a number of how many spaces and chars to move left or right
// when calling the datastructure logic make sure to skip the nulls
// --------------------------- Cursor Movement -------------------------------------
var count_rest_of_segment = function count_rest_of_segment (direction, chr_index) {
  var sum_chars = null;
  //direction = 0 //back /left
  //direction = 1 //forward /right
  if(direction == 1)
  {
    sum_chars += chr_index;
    var prev_size = previous_line_segment.text().length;
    sum_chars += prev_size - previous_segment_index;
  }
  else if(direction == 0)
  {
    //alert('psi: ' + previous_segment_index);
    sum_chars += previous_segment_index;
    var curr_size = current_line_segment.text().length - chr_index;
    sum_chars += curr_size;
  }
  return sum_chars;
}

//Func: counts the untouched segments inbetween the two focuesed segments
//quirk: does not offer wrap around for multi line / multi para
var count_inbetween_segments = function count_inbetween_segments (direction){
  var sum_chr_count = 0;
  var cs = current_line_segment;
  var ps = previous_line_segment; //check exceptions
  var all_segments = null;
  if(direction == 1){
    all_segments = ps.nextUntil(cs, '.line-segment');
  }
  else if(direction ==0){
    all_segments = ps.prevUntil(cs, '.line-segment');
  }
  all_segments.each(function(){
    sum_chr_count += $(this).text().length;
  });
  return sum_chr_count;
}

//This fun replaces the above one to calculate remainder segment length
//if the span is accross lines
var count_outlier_segments = function count_outlier_segments (direction){
  // body...
  var sum_chr_count = 0;
  var prev = previous_line_segment;
  var curr = current_line_segment;

  if(direction == 1){
    var prev_segs = prev.nextAll('.line-segment');
    var curr_segs = curr.prevAll('.line-segment');

    prev_segs.each(function (itt){
      sum_chr_count += $(this).text().length;
    });

    curr_segs.each(function(itt){
      sum_chr_count += $(this).text().length;
    });

  }else if(direction == 0){
    var prev_segs = curr.nextAll('.line-segment');
    var curr_segs = prev.prevAll('.line-segment');


    prev_segs.each(function (itt){
      sum_chr_count += $(this).text().length;
    });

    curr_segs.each(function(itt){
      sum_chr_count += $(this).text().length;
    });

  }

  return sum_chr_count;
}

var count_outlier_lines = function count_outlier_lines (direction) {
  var sum_chr_count = 0;
  if(direction == 1){
    var all_segs = previous_line.nextAll('.line').children('.line-segment');
    var all_segs2 = current_line.prevAll('.line').children('.line-segment');

    all_segs.each(function (itt){
      sum_chr_count += $(this).text().length;

    });
    all_segs2.each(function (itt){
      sum_chr_count += $(this).text().length;
    });
  }

  else if(direction == 0){
    var all_segs = current_line.nextAll('.line').children('.line-segment');
    var all_segs2 = previous_line.prevAll('.line').children('.line-segment');

    all_segs.each(function (itt){
      sum_chr_count += $(this).text().length;
    });

    all_segs2.each(function (itt){
      sum_chr_count += $(this).text().length;
    });
  }

  return sum_chr_count;
}

//EDIT
//counts remaining segments for last line on both sides
var count_remaining_segments = function count_remaining_segments (direction) {
  var sum_chr_count = null;
  if(direction ==1){
    var remaining_segments_curr = current_line.children('.line-segment').first();
    var remaining_segments_prev = previous_line.children('.line-segment').last();
    sum_chr_count += remaining_segments_curr.text().length;
    sum_chr_count += remaining_segments_prev.text().length;
    var all_segments = remaining_segments_curr.nextUntil(current_line_segment);
    var all_segments_prev = remaining_segments_prev.prevUntil(previous_line_segment);

    all_segments.each(function(itt){
           sum_chr_count += $(this).text().length;
         });
    all_segments_prev.each(function(itt){
           sum_chr_count += $(this).text().length;
         });
  }

  if(direction ==0){
    var remaining_segments_curr = current_line.children('.line-segment').last();
    var remaining_segments_prev = previous_line.children('.line-segment').first();
    sum_chr_count += remaining_segments_curr.text().length;
    sum_chr_count += remaining_segments_prev.text().length;

    var all_segments = remaining_segments_curr.prevUntil(current_line_segment);
    var all_segments_prev = remaining_segments_prev.nextUntil(previous_line_segment);

    all_segments.each(function(itt){
           sum_chr_count += $(this).text().length;
         });
    all_segments_prev.each(function(itt){
           sum_chr_count += $(this).text().length;
         });
  }
  return sum_chr_count;
}

var count_inbetween_lines = function count_inbetween_lines (direction){
  var sum_chr_count = 0;
  if(direction == 1){
    var all_segs = previous_line.nextUntil(current_line,'.line').children('.line-segment');
    sum_chr_count = all_segs.length;
  }else if(direction == 0){
    var all_segs = current_line.nextUntil(previous_line, '.line').children('.line-segment');
    sum_chr_count = all_segs.length;
  }
  return sum_chr_count;
}


//same as the method under except it does not iterate through paras to make if faster
var tobefixed = function in_para_segment_count (direction) {
  var sum_chr_count = null;

  //from begging line of para count all lines until selected
  if(direction == 1)
  {
    //count initial child line
    var initial_child_line = current_para.children('.line').first();

    if(!initial_child_line.is(current_line)){
      icl = initial_child_line.find('.line-segment');
      icl.each(function(itt){
            sum_chr_count += $(this).text().length;
          });

      var lines_involved = initial_child_line.nextUntil(current_line);

      lines_involved.each(function( index ){
      var all_line_seg = $(this).find('.line-segment');
        all_line_seg.each(function(itt){
           sum_chr_count += $(this).text().length;
         });
      });
    }

    var initial_child_line_prev = previous_para.children('.line').last()
    if(!initial_child_line_prev.is(previous_line)){
      icl = initial_child_line_prev.find('.line-segment');
      icl.each(function(itt){
            sum_chr_count += $(this).text().length;
          });

      var lines_involved = initial_child_line_prev.prevUntil(previous_line);

      lines_involved.each(function( index ){
      var all_line_seg = $(this).find('.line-segment');
        all_line_seg.each(function(itt){
           sum_chr_count += $(this).text().length;
         });
      });

    }
  }

  //from begging line of para count all lines until selected
  if(direction == 0)
  {
    var last_child_line = current_para.children('.line').last();
    if(!last_child_line.is(current_line)){
      icl = last_child_line.find('.line-segment');
      icl.each(function(itt){
            sum_chr_count += $(this).text().length;
          });

      var lines_involved = last_child_line.nextUntil(current_line);
      lines_involved.each(function( index ){
      var all_line_seg = $(this).find('.line-segment');
      all_line_seg.each(function(itt){
          sum_chr_count += $(this).text().length;
        });
      });
    }


    var initial_child_line_prev = previous_para.children('.line').first()
    if(!initial_child_line_prev.is(previous_line)){
      icl = initial_child_line_prev.find('.line-segment');
      icl.each(function(itt){
            sum_chr_count += $(this).text().length;
          });

      var lines_involved = initial_child_line_prev.nextUntil(previous_line);

      lines_involved.each(function( index ){
      var all_line_seg = $(this).find('.line-segment');
        all_line_seg.each(function(itt){
           sum_chr_count += $(this).text().length;
         });
      });
    }
  }
  return sum_chr_count;
}

//also need logic somewhere in this area to deal with neighbouring paras
//it will be th last call in the count_inbetween_segments chain
var count_inbetween_paras = function count_inbetween_paras (direction) {
  //get all the segmets inbetween and count their chars

  var sum_chr_count = null;
  //we are moving the gap right
  if(direction == 1)
  {
    //calculate in prev_segment_level
    //var paras_involved =
    //Care nextUntil is cyclical
    var paras_involved = previous_para.nextUntil(current_para);

    paras_involved.each(function( index ) {
        var all_line_seg = $(this).find('.line-segment');
        all_line_seg.each(function(itt){
        sum_chr_count += $(this).text().length;
        });
    });
    //alert(sum_chr_count);
  }

  //we are moving the gap left
  if(direction == 0)
  {
    var paras_involved = previous_para.prevUntil(current_para);

    paras_involved.each(function( index ) {
        var all_line_seg = $(this).find('.line-segment');
        all_line_seg.each(function(itt){
        sum_chr_count += $(this).text().length;
        });
    });
  }
  return sum_chr_count;
}

//New method to work with back space
var back_space_move_left = function back_space_move_left(e){


}


//Func: takes in click location e and moves cursor to that position
//Func: All it does is move cursor to physical location
//Return: The index of the char that is after the cursor
//Quirk: will always move segment if the seg_index is last for
var jump_cursor_in_segment = function jump_cursor_in_segment (e) {
  // the click logic
  current_line_segment.css({'background-color':'green'});
  //alert("-"+current_line_segment.text()+"-");
  //posX is the real physical location of the segment starting
  //from the very left point
  //Allows to figure out if event  is to the left or right of the
  //segment in question
  var posX = current_line_segment.offset().left;
  var Xpos = e.pageX - posX;
  //alert("Xpos "+ Xpos + " E.pageX " + e.pageX);
  //$(".check-width").css = current_line_segment.css;
  var count = current_line_segment.text().length;
  var seg_text = current_line_segment.text().split('');
  var prev_char = null;
  var prev_width = null;
  var accumulated_width = null;
  var seg_index = 0;

  var alertTest = '';
  for(var i = 0; i < seg_text.length; i++){
    alertTest += seg_text[i] + ' , ' ;
  }


  for(var i = 0; i < count; i++)
  {
    var this_char = null;
    var left_offset_boundary = accumulated_width;

    if(seg_text[i] == ' ')
    {
        this_char = "&nbsp;";
    }
    else
    {
        this_char = seg_text[i];
    }
    //alert(this_char);
    //add to single width
    $("#single-width").html(this_char);
    var curr_width = $("#single-width").width();

    if(prev_char != null)
    {
      if(multi_width != prev_width + curr_width) // multi-width is neccessary for quirks
      {
        $("#multi-width").html(prev_char + this_char);
        var multi_width = $("#multi-width").width();
        accumulated_width += multi_width - prev_width;
        //alert("sdas");
      }
      else
      {
        accumulated_width += curr_width;
      }
    }
    else
    {
      accumulated_width += curr_width;
    }

    if(accumulated_width > Xpos)
    {
        var right_offset = accumulated_width - Xpos;
        var left_offset = Xpos -left_offset_boundary;

        //right half off the chr
        if(right_offset < left_offset || right_offset == left_offset)
        {
          $(".cursor").css({'left': (accumulated_width - 1  + posX) + 'px'});
          var line_height = current_line_segment.height();
          $(".cursor").css({'height': line_height + 'px'});
          var line_top = current_line_segment.offset().top;
          $(".cursor").css({'top': line_top + 'px'});
          seg_index = i + 1;
        }
        //clicked on left half of the chr
        else
        {
          $(".cursor").css({'left': ( left_offset_boundary - 1 + posX) + 'px'});
          var line_height = current_line_segment.height();
          $(".cursor").css({'height': line_height + 'px'});
          var line_top = current_line_segment.offset().top;
          $(".cursor").css({'top': line_top + 'px'});
          //seg_index = i - 1;
          seg_index = i;
        }
      break;
    }
    //Determine correct binding segment
    //There will be three state
    //Beggining of line
    //Middle of line
    //End of line
    prev_char = this_char;
    prev_width = curr_width;
  }
  return seg_index;
}

// ERROR in some instances the back_space stem
var move_cursor_left = function move_cursor_left(chr){
  //alert('#'+chr+"#");
  var cls = current_line_segment.index();
  var special_case_adjust = 0;
  if(chr == ' ')
  {
    chr = "&nbsp;";
  }

  $("#single-width").html(chr);
  var curr_width = $("#single-width").width();
  var e = jQuery.Event( "mousedown", {
  which: 1,
    pageX: $(".cursor").offset().left - curr_width,
    pageY: $(".cursor").offset().top
  });

  jump_cursor_in_segment(e);
  //current_segment_index = jump_cursor_in_segment(e);

  //do i need to update the current line segment ??

  //click_cursor is not necessary because ??
  //click_cursor(current_segment_index);
}

//@current
var move_cursor_right = function move_cursor_right (chr) {
  //alert(current_segment_index);
  //Simple hack:: fake a click event of + width of curr char
  var cls = current_line_segment.index();
  var special_case_adjust = 0;

  if(chr == ' ')
  {
    chr = "&nbsp;";
  }
  //the char in question has been stored in memory
  $("#single-width").html(chr);
  var curr_width = $("#single-width").width();

  if(current_segment_index > 0){
    var length = $("#multi-width").html(current_line_segment.text().charAt(current_segment_index-1) + chr).width();
    var preceeding_char_length = $("#single-width").html(current_line_segment.text().charAt(current_segment_index-1)).width();
    special_case_adjust = (preceeding_char_length + curr_width) - length;
  }
  var e = jQuery.Event( "mousedown", {
  which: 1,
    pageX: $(".cursor").offset().left + curr_width - special_case_adjust,
    pageY: $(".cursor").offset().top
  });
  jump_cursor_in_segment(e);
}

var move_cursor_down = function move_cursor_down () {
}


var move_cursor_up = function move_cursor_up () {
}

//haven't implemented gap start
var move_cursor = function move_cursor (chr_index) {
  //if segment is the same
  //alert(current_line_segment.data() + '  ' + previous_line_segment);
  //alert("gap start: " + gap_start + " gap end" + gap_end);

  if(current_line_segment.is(previous_line_segment))
  {
    //alert("hello");
    if(chr_index > previous_segment_index)
    {
      move_multiple_forward(chr_index - previous_segment_index)
    }
    if(chr_index < previous_segment_index)
    {
      move_multiple_back(previous_segment_index - chr_index);
    }
    if(chr_index == previous_segment_index)
    {
    }
  }
}
//commit index changes


//This method synchronises the data structure with the location
//of the cursor in physical space
var click_cursor = function click_cursor(chr_index)
{
  //in future count lines will not be called
  //as length of each line will be store in the
  //critical counter buffer. [optimisation phase]

  current_line = current_line_segment.parent();
  current_para = current_line.parent();

  var charCounter = null;

  // if still on the same segment
  if(current_line_segment.is(previous_line_segment))
  {
    //where in the segment did i click?
    //chr_index = jump_cursor_in_segment(e);
    //is it greater than previous?
    if(chr_index > previous_segment_index)
    {
      //move gap forward
      charCounter = chr_index - previous_segment_index;
      move_multiple_forward(charCounter);
    }
    //is it less than previous ?
    else if(chr_index < previous_segment_index)
    {
      //move gap back
      charCounter = previous_segment_index - chr_index;
      move_multiple_back(charCounter);
    }
    //do nothing -- exit execution as fast as possible
    else if(chr_index == previous_segment_index)
    {
      //do nothing
    }
  }
  //if still on the same line
  else if (previous_line.is(current_line))
  {
    //move gap right
    if(current_line_segment.index() > previous_line_segment.index())
    {

      charCounter = count_rest_of_segment(1 , chr_index);
      charCounter += count_inbetween_segments(1);
      move_multiple_forward(charCounter);

    }
    //move gap left
    else if(current_line_segment.index() < previous_line_segment.index())
    {
      var charCounter1 = count_rest_of_segment(0 , chr_index);
      var charCounter2 = count_inbetween_segments(0);
      move_multiple_back(charCounter2 + charCounter1);
    }
  }
  // if still on the same paragraph -- count in between lines
  else if(previous_para.is(current_para))
  {
    if(current_line.index() > previous_line.index())
    {
      charCounter = count_rest_of_segment(1 , chr_index);
      charCounter += count_outlier_segments(1);
      charCounter += count_inbetween_lines(1);
      move_multiple_forward(charCounter);
    }
    else if(current_line.index() < previous_line.index())
    {
      charCounter = count_rest_of_segment(0 , chr_index);
      charCounter += count_outlier_segments(0);
      charCounter += count_inbetween_lines(0);
      move_multiple_back(charCounter);
    }
  }

  //working with different paragrphs
  else if(!previous_para.is(current_para))
  {
    //chr_index = jump_cursor_in_segment(e);
    //alert("in different para");
    //gap will move right
    if(current_para.index() > previous_para.index())
    {
      charCounter = count_rest_of_segment(1 , chr_index);
      charCounter += count_outlier_segments(1);
      charCounter += count_outlier_lines(1);
      charCounter += count_inbetween_paras(1);
      move_multiple_forward(charCounter);
    }
    else if (current_para.index() < previous_para.index() ){
      charCounter = count_rest_of_segment(0 , chr_index);
      charCounter += count_outlier_segments(0);
      charCounter += count_outlier_lines(0);
      charCounter += count_inbetween_paras(0);
      move_multiple_back(charCounter);
    }
    //calculate sum of segments in in between paras
    //calculate sum of lines in between segments
    //calculate segment offsets
  }
  //configure vars for next event cycle
  //previous_segment_index = chr_index;
  //this backwards movement is not sticking
  previous_segment_index = chr_index;
  previous_line_segment = current_line_segment;
  previous_line = current_line;
  previous_para = current_para;
}


// THE BIG CHECKS ______________ YES
var check_out_of_bounds = function check_out_of_bounds () {
  var boundwidth = current_line.width();

  var child_combined_width = null;
  current_line.children('span').each(function () {
      var curr_item = $(this);
      child_combined_width += curr_item.width();
  });
  if(child_combined_width > boundwidth - 10)
  {
   return true;
  }
  else
  {
    return false;
  }
}

/*
$(document).on('click', '.module', function(e){
    alert("fired");
});

$(".module").click(function(e){
  alert("fired");
  var mod_left_mov = $(this).offset().left + $(this).outerWidth();
  $('.cursor').css({"left": mod_left_mov+ "px"});
});
*/


// ----------------------------End of Cursor movement ---------------------------
//-------------------------------------------------------------------------------
